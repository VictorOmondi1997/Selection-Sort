#include <iostream>
#include <conio.h>

#define MAX 10

using namespace std;

class selsort{
    int arr[MAX], n;
public:
    void getData();
    void showData();
    void sortLogic();
};
void selsort::getData(){
    cout<< "How Many Elements You Require: ";
    cin>> n;
    for(int i=0; i<n; i++){
        cout<< "Enter Element in Cell "<<i <<":\t";
        cin>>arr[i];
    }
}
void selsort::showData(){
    cout<< "\n--Sorted Elements--\n";
    for(int i=0; i<n; i++)
        cout<< arr[i]<< " ";
}
void selsort::sortLogic(){
    int temp, min;
    for(int i=0;i<n;i++){
        min = i;
        for(int j=i+1; j<n; j++){
            if(arr[min]>arr[j]){
                min = j;
            }
        }
        temp = arr[min];
        arr[min] = arr[i];
        arr[i] = temp;
        cout<< "\n arr[min] = "<< arr[min]<< "\tarr[i] = "<< arr[i];
    }
}

int main()
{
    cout << "\n*****Selection Sort!*****\n";
    selsort obj;
    obj.getData();
    obj.sortLogic();
    obj.showData();
}
